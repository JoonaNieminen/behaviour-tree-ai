﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyState
{
    PATROL,
    CHASE
}

public class EnemyController : MonoBehaviour
{
    private NavMeshAgent navAgent;
    private EnemyState enemyState;
    private Transform target;

    public float walkSpeed = 20f;
    public float runSpeed = 100f;

    //how far the AI will detect the target from
    public float chaseDistance = 7f;

    //randomized patrolling area
    public float patrolRadiusMin = 20f, patrolRadiusMax = 60f;

    //how many seconds the AI patrols before finding a new random spot to head to
    public float patrolForThisTime = 15f;
    //tracks the patroltimer
    private float patrolTimer;

    private void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
        target = GameObject.FindWithTag("Target").transform;
    }

    void Start()
    {
        enemyState = EnemyState.PATROL;
        patrolTimer = patrolForThisTime;
    }
    
    void Update()
    {
        if(enemyState == EnemyState.PATROL)
        {
            Patrol();
        }

        if(enemyState == EnemyState.CHASE)
        {
            Chase();
        }
    }

    void Patrol()
    {
        navAgent.speed = walkSpeed;

        patrolTimer += Time.deltaTime;

        if(patrolTimer > patrolForThisTime)
        {
            SetNewRandomDestination();
            patrolTimer = 0f;
        }

        //attack the target if the AI is close enough
        if(Vector3.Distance(transform.position, target.position) <= chaseDistance)
        {
            enemyState = EnemyState.CHASE;

            //stops the movement before attacking to prevent sliding past the target
            navAgent.isStopped = true;
        }
        
    }

    void Chase()
    {
        //wait until stopped before charging the target
        if(navAgent.velocity.sqrMagnitude == 0)
        {
            navAgent.isStopped = false;
        }

        navAgent.speed = runSpeed;

        navAgent.SetDestination(target.position);
    }

    void SetNewRandomDestination()
    {
        float randomRadius = Random.Range(patrolRadiusMin, patrolRadiusMax);

        Vector3 randomDirection = Random.insideUnitSphere * randomRadius;
        randomDirection += transform.position;

        NavMeshHit navHit;

        //gets the random point, compares it to the navigable area, if that point is outside the navigable area, calculates a new point
        NavMesh.SamplePosition(randomDirection, out navHit, randomRadius, -1);

        //navHit contains the value from the previous function cuz it was called with 'out'
        navAgent.SetDestination(navHit.position);
    }
}
