﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScript : MonoBehaviour
{

    public GameObject target;
    public float placeX, placeZ;

    // Start is called before the first frame update
    void Start()
    {
        placeX = Random.Range(-220, 230);
        placeZ = Random.Range(-223, 236);
        target.transform.position = new Vector3(placeX, 16, placeZ);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
